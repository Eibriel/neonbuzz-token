# NeonBuzz Token

Contract for NeonBuzz NFT


2D image planes with unique neon signs to include in your virtual worlds! Compatible with Cryptovoxels (more worlds to come in the future)


```
clone https://gitlab.com/Eibriel/neonbuzz-token.git

cd neonbuzz-token/

yarn init

yarn add openzeppelin-solidity

yarn add truffle

yarn add @truffle/hdwallet-provider


export INFURA_KEY=""

export MNEMONIC=""

export NETWORK=""


truffle deploy --network rinkeby
```

Rinkeby Contract: 0x79BBe2A433bfa585C7Ac3B753002A18a51E680e4

248741
3423472
225237
2915716

6813166

Mainnet Contract: 0xFC3D514AC7ec6371A5c389c5A9D4a69A6736C0AE

248741
3423472
225237
2915728

`date +%s`

```

export OWNER_ADDRESS=""

export NETWORK=""

export NFT_CONTRACT_ADDRESS=""

export TOKENURI=""

node scripts/mint.js

```

```
yarn add truffle-flattener

./node_modules/.bin/truffle-flattener contracts/NeonBuzz.sol > flattened/NeonBuzz.sol
```

0: QmXLGq9LWQfohKTg6BuaYxrncT7uYbSKpDXaEYrPiSSwK2

00: QmTh7m494uyhhki9ct9Gn9HF2RNcZx1rCgcfYEjeuxd1RC

2: Qmd9yrobTu4ic6z7cHNWsLDCgadA6vArJ6bRRfx5E84idT
