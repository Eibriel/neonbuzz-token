pragma solidity ^0.5.0;

import "./TradeableERC721Token.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

/**
 * @title Creature
 * Creature - a contract for my non-fungible creatures.
 */
contract NeonBuzz is TradeableERC721Token {
  constructor(address _proxyRegistryAddress) TradeableERC721Token("NeonBuzz", "NBUZ", _proxyRegistryAddress) public {  }
}
